package com.epam.training;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Sorting sorting = new Sorting();
        int[] arrayToPrint = sorting.sort(sorting.parse(args));

        System.out.println(Arrays.toString(arrayToPrint));
    }
}