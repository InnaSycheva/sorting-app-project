package com.epam.training;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The program implements an application
 * that accepts an array as input
 * and outputs its sorted version
 */
public class Sorting {

    /**
     * Convert String array to integer array
     *
     * @param stringArray is the passed argument in string format that needs to be converted to an integer
     * @return the integer array obtained from a string
     * @throws IllegalArgumentException if your input format is invalid
     */
    public int[] parse(String[] stringArray) {
        Logger logger = Logger.getLogger(Main.class.getName());
        logger.setLevel(Level.ALL);

        if (stringArray.length == 0) {
            logger.warning("Missing input data");
            throw new IllegalArgumentException();
        }
        if (stringArray.length > 10) {
            logger.warning("The allowed number of arguments has been exceeded");
            throw new IllegalArgumentException();
        }

        int[] array = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            array[i] = Integer.parseInt(stringArray[i]);
        }

        return array;
    }

    /**
     * Sorts the integer array
     *
     * @param newArray is integer array to be sorted
     * @return is the integer array sorted in ascending order
     * @throws IllegalArgumentException if your input format is invalid
     */
    public int[] sort(int[] newArray) {
        if (newArray == null) {
            throw new IllegalArgumentException();
        }
        if (newArray.length > 10) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i < newArray.length - 1; i++) {
            for (int j = i + 1; j < newArray.length; j++) {

                if (newArray[i] > newArray[j]) {
                    int temp = newArray[i];
                    newArray[i] = newArray[j];
                    newArray[j] = temp;
                }
            }
        }

        return newArray;
    }
}
