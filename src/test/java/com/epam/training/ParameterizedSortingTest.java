package com.epam.training;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class ParameterizedSortingTest {
    Sorting sorting = new Sorting();

    int[] actual;
    int[] expected;

    public ParameterizedSortingTest(int[] actual, int[] expected) {
        this.actual = actual;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection <Object[]> data() {

        return Arrays.asList(new Object[][]{
                {
                        new int[]{10, 7, 8, 9, 5, 1, 4, 2, 6, 3},
                        new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
                },
                {
                        new int[]{10, 7, 8, 9, 5, 1, 4, 2, 6},
                        new int[]{1, 2, 4, 5, 6, 7, 8, 9, 10}
                },
                {
                        new int[]{10, 7, 8, 9, 5, 1, 4, 2},
                        new int[]{1, 2, 4, 5, 7, 8, 9, 10}
                },
                {
                        new int[]{10, 7, 8, 9, 5, 1, 4},
                        new int[]{1, 4, 5, 7, 8, 9, 10}
                },
                {
                        new int[]{10, 7, 8, 9, 5, 1},
                        new int[]{1, 5, 7, 8, 9, 10}
                },
                {
                        new int[]{10, 7, 8, 9, 5},
                        new int[]{5, 7, 8, 9, 10}
                },
                {
                        new int[]{10, 7, 8, 9},
                        new int[]{7, 8, 9, 10}
                },
                {
                        new int[]{10, 7, 8},
                        new int[]{7, 8, 10}
                },
                {
                        new int[]{10, 7},
                        new int[]{7, 10}
                },
        });
    }

    @Test
    public void testCase() {
        assertArrayEquals(expected, sorting.sort(actual));
    }
}
