package com.epam.training;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ParseTest {
    Sorting sorting = new Sorting();
    @Test (expected = NullPointerException.class)
    public void testNullCase(){
        sorting.parse(null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testEmptyCase(){
        sorting.parse(new String[] {""});
    }

    @Test (expected = NumberFormatException.class)
    public void testNotIntegerElementCase(){
        sorting.parse(new String[] {"one"});
    }

    @Test
    public void testSingleElementCase(){
        String[] actual = new String[] {"1"};
        assertArrayEquals(new int[] {1}, sorting.parse(actual));
    }

    @Test
    public void testNormalCase(){
        String[] actual = new String[] {"1", "2", "5"};
        assertArrayEquals(new int[] {1,2,5}, sorting.parse(actual));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMoreCase(){
        String[] actual = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
        sorting.parse(actual);
    }
}
