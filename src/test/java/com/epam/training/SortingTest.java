package com.epam.training;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class SortingTest {
    Sorting sorting = new Sorting();
    @Test (expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }

    @Test
    public void testEmptyCase(){
        assertArrayEquals(new int[] {}, sorting.sort(new int[]{}));
    }

    @Test
    public void testSingleElementArrayCase() {
        assertArrayEquals(new int[] {1}, sorting.sort(new int[] {1}));
    }

    @Test
    public void testSortedArraysCase() {
        int[] actual = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        sorting.sort(actual);
        assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, actual);
    }

    @Test
    public void test10Case() {
        int[] actual = new int[] {10, 7, 8, 9, 5, 1, 4, 2, 6, 3};

        assertArrayEquals(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, sorting.sort(actual));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testMoreCase(){
        int[] actual = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        sorting.sort(actual);
    }





}


